import asyncio
import os
import shlex
import subprocess
import sys

import pytest

import aiohglib

# Those environment variables disable all Mercurial extensions
HG_ENV = {"HGRCPATH": "/dev/null", "HGUSER": "Test Test <test@localhost>"}

HG_REPO_SINGLE = {
    "single": ["touch default.txt", "hg ci -A -m'initial commit'",],
}

HG_REPOS_SINGLE = {"single": HG_REPO_SINGLE}


@pytest.fixture
def hg_env(monkeypatch):
    # Applies environment variables as a fixture.
    for key, value in HG_ENV.items():
        monkeypatch.setenv(key, value)


@pytest.fixture(params=HG_REPOS_SINGLE)
def hg_repos_single(request, tmpdir_factory):
    root_path = tmpdir_factory.mktemp(request.param)
    return hg_repos(HG_REPOS_SINGLE[request.param], root_path)


def hg_repos(repos, root_path):
    names = []
    for repo, cmds in repos.items():
        names.append(repo)
        path = os.path.join(root_path, repo)
        subprocess.run(shlex.split(f"hg init {path}"), env=HG_ENV)
        for line in cmds:
            if line.startswith("hg "):
                subprocess.run(shlex.split(f"hg -R {path} {line[3:]}"), env=HG_ENV)
            elif line.startswith("touch "):
                with open(os.path.join(path, line.split()[1]), "wt") as fd:
                    fd.write(repo)
            elif line.startswith("append "):
                with open(os.path.join(path, line.split()[1]), "at") as fd:
                    fd.write(line.split()[2])
            else:
                assert False, f"unknown cmd: {line}"
    return root_path, names


@pytest.mark.asyncio
async def test_client_open(hg_env, hg_repos_single):
    root_path, repos = hg_repos_single
    assert len(repos) == 1
    path = os.path.join(root_path, repos[0])
    async with aiohglib.open(path) as client:
        log = await client.log(revrange="tip")
        assert len(log) == 1
        assert log[0].desc == "initial commit"
